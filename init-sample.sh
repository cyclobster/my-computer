#!/bin/bash

# Get distribution codename
source /etc/lsb-release

# Download and install puppet
wget https://apt.puppetlabs.com/puppet-release-${DISTRIB_CODENAME}.deb
dpkg -i puppet-release-${DISTRIB_CODENAME}.deb
apt-get update
apt install -y git puppet-agent

# Add defaults to puppet user
echo 'Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/opt/puppetlabs/bin:/opt/puppetlabs/puppet/bin"' >/etc/sudoers.d/puppet

# Install puppet gems
/opt/puppetlabs/puppet/bin/gem install gpgme --no-rdoc --no-ri
/opt/puppetlabs/puppet/bin/gem install hiera-eyaml-gpg --no-rdoc --no-ri
/opt/puppetlabs/puppet/bin/gem install r10k --no-rdoc --no-ri

# Get Manifests
rm -rf /etc/puppetlabs/code/environments/production
git clone https://gitlab.com/cyclobster/my-computer.git /etc/puppetlabs/code/environments/production
cd /etc/puppetlabs/code/environments/production && git pull

# Initialize
/opt/puppetlabs/bin/puppet apply manifests/site.pp
