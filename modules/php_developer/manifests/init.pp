class php_developer ( Array $packages ){
	$packages.each | Hash $package | {
	  package { $package['name']:
		ensure => $package['ensure'],
	  }
	}
}
